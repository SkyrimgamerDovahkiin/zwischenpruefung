using UnityEngine;
using UnityEngine.UI;

public class NewGameButton : MonoBehaviour
{
    public bool clicked = false;
    public MainMenu menu;
    public GameObject mainMenuBackground;

    public void StartGame()
    {
        Time.timeScale = 0f;
        if (clicked == false)
        {
            clicked = true;
            mainMenuBackground.SetActive(false);
            menu.healthBar.SetActive(true);
            menu.progressbar.SetActive(true);
            menu.story.SetActive(true);
            menu.keyPressed = false;
            Destroy(this);
            gameObject.GetComponent<Button>().enabled = false;            
        }
    }
}