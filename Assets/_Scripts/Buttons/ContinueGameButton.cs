using UnityEngine;
public class ContinueGameButton : MonoBehaviour
{
    public bool clicked = false;
    public MainMenu menu;
    public GameObject mainMenuBackground;
    public NewGameButton ngb;

    public void ContinueGame()
    {
        Time.timeScale = 0f;
        if (ngb.clicked)
        {
            Time.timeScale = 1f;
            clicked = true;
            mainMenuBackground.SetActive(false);
            menu.healthBar.SetActive(true);
            menu.progressbar.SetActive(true);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            menu.keyPressed = false;
        }
    }
}