using UnityEngine;

public class SettingsBackButton : MonoBehaviour
{
    public GameObject backgroundMM;
    public GameObject settings;
    public void GoBack()
    {
        settings.SetActive(false);
        backgroundMM.SetActive(true);
    }
}
