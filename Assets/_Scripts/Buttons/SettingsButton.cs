using UnityEngine;
using UnityEngine.UI;

public class SettingsButton : MonoBehaviour
{
    public GameObject backgroundMM;
    public GameObject settings;
    public void GoToSettings()
    {
        backgroundMM.SetActive(false);
        settings.SetActive(true);
    }
}
