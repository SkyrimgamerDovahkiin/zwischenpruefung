using UnityEngine;
using UnityEditor;

public class QuitButton : MonoBehaviour
{
    public void QuitGame()
    {
        //EditorApplication.isPlaying = false;
        Application.Quit();
    }
}
