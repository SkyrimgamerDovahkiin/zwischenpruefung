using UnityEngine;
using UnityEngine.VFX;

public class PlayerActivateTorchlight : MonoBehaviour
{
    Light torchlight;
    public static int lightActivated;
    public Component[] fires;

    private void Start() 
    {
        torchlight = GetComponentInChildren<Light>();        
    }

    void OnTriggerEnter(Collider other) 
    {
        torchlight.enabled = true;
        transform.GetComponentInChildren<SphereCollider>().enabled = true;
        fires = GetComponentsInChildren<VisualEffect>();

        //Aktiviert die Feuereffekte der Kerzen
        foreach (VisualEffect fire in fires)
            fire.enabled = true;
        
        lightActivated++;
        Destroy(this);
    }
}
