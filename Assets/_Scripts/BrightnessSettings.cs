using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.HighDefinition;

public class BrightnessSettings : MonoBehaviour
{
    Light sun;
    public Slider sunSlider;
    HDAdditionalLightData lightData;

    void Start()
    {
        sun = GameObject.Find("Sun").GetComponent<Light>();
        lightData = sun.GetComponent<HDAdditionalLightData>();
    }

    public void ChangeMainLight()
    {
        lightData.intensity = sunSlider.value;
    }
}
