using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthSystem : MonoBehaviour
{
    public float maxHealth = 100;
    public float damage = 0.01f;
    public float regeneration = 0.01f;
    public static float currentHealth;
    public Slider damageSlider;
    public Slider regenerationSlider;
    [SerializeField] GameObject regen;
    Image healthbar;
    PlayerInSafeZone pisz;
    [SerializeField] TextMeshProUGUI youLostText;
    [SerializeField] TextMeshProUGUI youWonText;

        void Start()
    {
        healthbar = GetComponent<Image>();
        currentHealth = maxHealth;
        SetMaxHealth(maxHealth);
    }
    void Update()
    {
        if(!PlayerInSafeZone.playerInSafeZone && !youWonText.enabled)
        {
            currentHealth -= damage;
        }

        if(PlayerInSafeZone.playerInSafeZone)
        {
           currentHealth += 0f;
           if (regen.activeSelf)
           {
                currentHealth += regeneration;
           }
        }

        SetHealth(currentHealth);

        if (currentHealth < 0f)
        {
            currentHealth = 0f;        
        }
        else if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        if (currentHealth == 0f && !youWonText.enabled)
        {
            youLostText.enabled = true;
        }
    }

    public void SetMaxHealth(float health)
    {
        healthbar.fillAmount = health / maxHealth;
    }

    public void SetHealth(float health)
    {
        healthbar.fillAmount = health / maxHealth;
    }

    public void ChangeDamageValue()
    {
        damage = damageSlider.value;
    }

    public void ChangeRegenValue()
    {
        regeneration = regenerationSlider.value;
    }
}
