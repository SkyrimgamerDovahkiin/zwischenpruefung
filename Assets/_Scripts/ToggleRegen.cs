using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleRegen : MonoBehaviour
{
    [SerializeField] GameObject regen;

    public void EnableRegen()
    {
        regen.SetActive(!regen.activeSelf);
    }
}
