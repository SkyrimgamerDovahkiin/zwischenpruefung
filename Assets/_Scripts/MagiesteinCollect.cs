using UnityEngine;
using TMPro;

public class MagiesteinCollect : MonoBehaviour
{
    public TextMeshProUGUI youWonText;
    
    void OnTriggerEnter(Collider other) 
    {
        gameObject.SetActive(false);
        youWonText.enabled = true;
    }
}
