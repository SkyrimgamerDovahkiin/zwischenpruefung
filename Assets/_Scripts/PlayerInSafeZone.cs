using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInSafeZone : MonoBehaviour
{
    public static bool playerInSafeZone;

    void OnTriggerStay(Collider other) 
    {
        playerInSafeZone = true;
    }
    
    void OnTriggerExit(Collider other) 
    {
        playerInSafeZone = false;
    }
}
