using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public KeyCode mainMenuKey;
    [HideInInspector] public bool keyPressed = false;
    public GameObject mainMenuBackground;
    public GameObject healthBar;
    public GameObject progressbar;
    public GameObject story;
    public NewGameButton ngb;
    public ContinueGameButton cgb;
    public GameObject buttonContinue;

    void Start() 
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        keyPressed = false;
        mainMenuBackground.SetActive(true);
        healthBar.SetActive(false);
        progressbar.SetActive(false);
        story.SetActive(false);
        Time.timeScale = 0f;
        buttonContinue.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(mainMenuKey) && !keyPressed && (ngb.clicked || cgb.clicked))
        {
            mainMenuBackground.SetActive(true);
            healthBar.SetActive(false);
            progressbar.SetActive(false);
            keyPressed = true;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
            Time.timeScale = 0f;
            buttonContinue.SetActive(true);
        }

        else if (Input.GetKeyDown(mainMenuKey) && keyPressed)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            mainMenuBackground.SetActive(false);
            healthBar.SetActive(true);
            progressbar.SetActive(true);
            keyPressed = false;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1f;
        }
    }
}
