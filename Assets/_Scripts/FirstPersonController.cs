﻿using UnityEngine;
public class FirstPersonController : MonoBehaviour
{
    Transform camTransform;
    public float turnSpeed = 5.0f;
    GameObject player;
    Transform playerTransform;     
    Vector3 offset;
    [SerializeField] GameObject mainMenu;
    [SerializeField] GameObject settings;
    [SerializeField] GameObject story;

    void Start()
    {
        player = gameObject;
        playerTransform = player.transform;
        offset = new Vector3(3, 0, 3);
        Camera cam = Camera.main;
        camTransform = cam.transform;
    }

    void Update()
    {
        if(!settings.activeSelf && !mainMenu.activeSelf && !story.activeSelf && HealthSystem.currentHealth > 0f)
        {
            //Tasten zum Bewegen (Vorwärts, Seitwärts, Hoch, Runter)
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(camTransform.transform.forward * 0.1f);
            }
     
            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(-camTransform.transform.forward * 0.1f);
            }
    
            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(-camTransform.transform.right * 0.1f);
            }
     
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(camTransform.transform.right * 0.1f);
            }
    
            if (Input.GetKey(KeyCode.Q))
            {
                transform.Translate(camTransform.transform.up * 0.1f);
            }
     
            if (Input.GetKey(KeyCode.E))
            {
                transform.Translate(-camTransform.transform.up * 0.1f);
            }
    
            //Rotationsfunktion
            offset = Quaternion.AngleAxis (Input.GetAxis("Mouse X") * turnSpeed, Vector3.up) * offset;
            camTransform.transform.position = playerTransform.position + offset;
            camTransform.transform.LookAt(playerTransform.position);
        }
    }
}