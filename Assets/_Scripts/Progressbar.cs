using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Progressbar : MonoBehaviour
{
    Image progressbar;
    int progress = 0;
    int progressTotal = 0;
    public GameObject emptyParent;
    [SerializeField] GameObject magiestein;
    bool activated = false;
    
    private void Start() 
    {
        progressbar = GetComponent<Image>();
        progressTotal = emptyParent.transform.childCount;
    }

    void Update() 
    {
        progress = PlayerActivateTorchlight.lightActivated;
        progressbar.fillAmount = progress / (float) progressTotal;
        if (progress == progressTotal && !activated)
        {
            magiestein.SetActive(true);
            activated = true;
        }        
    }
}
