using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour 
{
    public AudioMixer MainMixer;
    public AnimationCurve bgMusicVolCurve;
    public AnimationCurve mainMenuMusicVolCurve;
    public Slider VMMSlider;
    float vMMSliderVol;
    public Slider VBGSlider;
    public NewGameButton ngb;
    public AudioSource bgMusic;
    float vBGSliderVol;
    bool muted = false;
    static Stack<AudioSource> sfxPool = new Stack<AudioSource>(30);

    void Start() 
    {
        ngb = GameObject.Find("Neues Spiel").GetComponent<NewGameButton>();
    }

    public void Update() 
    {
        if (ngb.clicked)
        {
            MainMixer.SetFloat("MainMenuMusicVol", -80f);
            bgMusic.enabled = true;
        }
    }

    public void SetMainMenuMusicVol(float mmMusicVol) 
    {
        float mmvol = mainMenuMusicVolCurve.Evaluate(mmMusicVol);
        MainMixer.SetFloat("MainMenuMusicVol", mmvol);
    }
    public void SetBGMusicVol(float bgMusicVol)
    {
        float bgvol = bgMusicVolCurve.Evaluate(bgMusicVol);
        MainMixer.SetFloat("BGMusicVol", bgvol);
    }

    public void MuteMainMenuMusic() 
    {
        if (!muted)
        {
            MainMixer.SetFloat("MainMenuMusicVol", -80f);
            vMMSliderVol = VMMSlider.value;
            VMMSlider.value = -80f;
            muted = true;
        }
        else if (muted)
        {
            MainMixer.SetFloat("MainMenuMusicVol", vMMSliderVol);
            VMMSlider.value = vMMSliderVol;
            muted = false;
        }
    }

    public void MuteBGMusic() 
    {
        if (!muted)
        {
            MainMixer.SetFloat("BGMusicVol", -80f);
            vBGSliderVol = VBGSlider.value;
            VBGSlider.value = -80f;
            muted = true;
        }
        else if (muted)
        {
            MainMixer.SetFloat("BGMusicVol", vBGSliderVol);
            VBGSlider.value = vBGSliderVol;
            muted = false;
        }
    }
}
